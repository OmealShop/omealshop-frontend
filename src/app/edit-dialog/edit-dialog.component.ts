import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {ProductService} from '../services/product.service';
import {Constants} from '../shared/constants';

@Component({
    selector: 'app-edit-dialog',
    templateUrl: './edit-dialog.component.html',
    styleUrls: ['./edit-dialog.component.scss']
})
export class EditDialogComponent implements OnInit {
    readonly userHouseHold = Constants.getFirstUserHouseHold();

    form: FormGroup;
    response: Subscription;


    constructor(public dialogRef: MatDialogRef<EditDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any, private user: FormBuilder,
                private snackBar: MatSnackBar, private productProvider: ProductService) {
    }

    ngOnInit() {
        this.form = this.user.group({
            productId: new FormControl({value: this.data.productId, disabled: true}),
            quantity: [this.data.quantity, Validators.required],
            remark: [this.data.comment]
        });
    }


    closePopup(code) {
        this.dialogRef.close(code);
    }

    handleData(data, productIdModified) {
        if (this.form.invalid) {
            this.snackBar.open('Veuillez renseigner tous les champs', '', {
                duration: 2000
            });
            return;
        }
        const quantity = data.quantity;
        const remark = data.remark;

        const httpAction = this.getMethode(productIdModified, quantity, remark) as string;
        switch (httpAction) {
            case 'DELETE':
                this.response = this.productProvider.deleteProductList(productIdModified, this.userHouseHold).subscribe(
                    result => {
                        if (result) {
                            this.snackBar.open('Le produit a bien été supprimé', '', {
                                duration: 2000
                            });
                            this.closePopup(1);
                        } else {
                            this.snackBar.open('Produit non supprimé à la liste', '', {
                                duration: 2000
                            });
                        }
                    });
                break;
            case 'POST':
                this.response = this.productProvider.addProductList(productIdModified, this.userHouseHold, quantity, remark).subscribe(
                    result => {
                        if (result) {
                            this.snackBar.open('Le produit a bien été ajouté', '', {
                                duration: 2000
                            });
                            this.closePopup(1);
                        } else {
                            this.snackBar.open('Produit non ajouté à la liste', '', {
                                duration: 2000
                            });
                        }
                    });
                break;
            case 'PATCH':
                this.response = this.productProvider.updateProductList(productIdModified, this.userHouseHold, quantity, remark).subscribe(
                    result => {
                        if (result) {
                            this.snackBar.open('Le produit a bien été modifié', '', {
                                duration: 2000
                            });
                            this.closePopup(1);
                        } else {
                            this.snackBar.open('Produit non modifié dans la liste', '', {
                                duration: 2000
                            });
                        }
                    });
                break;
        }
    }

    private getMethode(productIdModified: Number, quantity: Number, remark: string) {
        if (quantity === 0) {
            return 'DELETE';
        } else {
            let isPresent = false;
            const myProductList = this.productProvider.myProductList;
            myProductList.forEach(productItem => {
                if (productIdModified === productItem.PRODUCTS_LIST_id_prod) {
                    isPresent = true;
                }
            });
            if (isPresent) {
                return 'PATCH';
            } else {
                return 'POST';
            }
        }
    }
}
