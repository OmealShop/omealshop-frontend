import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import {
    MatButtonModule,
    MatCheckboxModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSnackBarModule
} from '@angular/material';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';
import {EditDialogComponent} from './edit-dialog.component';

@NgModule({
    imports: [
        CommonModule,
        MatInputModule,
        SharedModule,
        MatCheckboxModule,
        MatButtonModule,
        MatFormFieldModule,
        FormsModule,
        ReactiveFormsModule,
        MatDialogModule,
        MatButtonModule,
        FlexLayoutModule,
        ReactiveFormsModule
    ],
    declarations: [EditDialogComponent]
})
export class EditDialogModule {}
