export class Product {
    PRODUCT_id: number;
    PRODUCT_name: string;
    PRODUCT_unit: string;
    PRODUIT_TYPE: string;
    PRODUCT_TYPE_ID: number;
    PRODUCT_QUANTITY: number;
    PRODUCT_COMMENT: string;
    constructor(
    PRODUCT_id: number,
    PRODUCT_name: string,
    PRODUCT_unit: string,
    PRODUIT_TYPE: string,
    PRODUCT_TYPE_ID: number,
    PRODUCT_QUANTITY: number,
    PRODUCT_COMMENT: string
) {
        this.PRODUCT_id = PRODUCT_id;
        this.PRODUCT_name = PRODUCT_name;
        this.PRODUCT_unit = PRODUCT_unit;
        this.PRODUIT_TYPE = PRODUIT_TYPE;
        this.PRODUCT_TYPE_ID = PRODUCT_TYPE_ID;
        this.PRODUCT_QUANTITY = PRODUCT_QUANTITY;
        this.PRODUCT_COMMENT = PRODUCT_COMMENT;
    }
}

