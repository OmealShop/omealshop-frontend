export class User {
    id: number;
    name: string;
    firstname: string;
    mail: string;
    pwd: string;
    state: number;
    creation_date: Date;
}
