import {Product} from './product';

export class ProductType {
    PRODUCTS_id: number;
    PRODUCTS_products: Product[];
    PRODUCTS_title: string;
}
