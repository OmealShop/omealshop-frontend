export class ProductList {
    PRODUCTS_LIST_id_prod: number;
    PRODUCTS_LIST_id_household: string;
    PRODUCTS_LIST_quantity: number;
    PRODUCTS_List_remark: string;

    constructor(
        PRODUCT_LIST_id_prod: number,
        PRODUCT_LIST_id_household: string,
        PRODUCT_LIST_quantity: number,
        PRODUCT_List_remark: string
    ) {
        this.PRODUCTS_LIST_id_prod = PRODUCT_LIST_id_prod;
        this.PRODUCTS_LIST_id_household = PRODUCT_LIST_id_household;
        this.PRODUCTS_LIST_quantity = PRODUCT_LIST_quantity;
        this.PRODUCTS_List_remark = PRODUCT_List_remark;
    }
}

