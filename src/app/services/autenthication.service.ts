import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {HttpModule, Http } from '@angular/http';
import { User } from '../models/user';
import {Observable, of} from 'rxjs';

@Injectable()
export class AuthenticationService {
    constructor(private http: HttpClient) { }

    /**
     * vérifie si l'utilisateur peut s'authentifier
     * @param username nom d'utilisateur
     * @param password mot de passe
     */
    login(username: string, password: string): Observable<any> {
            return this.http.post<any>('http://localhost:3000/login/', { email:  username, pwd: password})
            .pipe(
                map(result => result),
                catchError(err => of(false))
            );
    }
}
