import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {ProductList} from '../models/productList';

@Injectable()
export class ProductService {
    private _myProductList: ProductList[];

    constructor(private httpClient: HttpClient) {
    }

    /**
     * renvoie la liste de tous les produits
     */
    getAllProducts() {
        return this.httpClient.get('http://localhost:3000/products').pipe(
            catchError(this.handleError('getAllProducts', []))
        );
    }

    /**
     * renvoie la liste de produits de l'utilisateur
     */
    get myProductList(): ProductList[] {
        return this._myProductList;
    }

    /**
     * renvoie les produits associés à un foyer
     * @param idHousehold identifiant du foyer
     */
    getProductsHousehold(idHousehold: number) {
        const res = this.httpClient.get<any>('http://localhost:3000/user/' + idHousehold + '/productsList').pipe(
            catchError(err => of(false))
        );
        res.subscribe(results => {
            this._myProductList = [];
            const r = results['products'] as ProductList[];
            r.forEach(productItem => {
                this._myProductList.push(new ProductList(productItem.PRODUCTS_LIST_id_prod,
                    productItem.PRODUCTS_LIST_id_household,
                    productItem.PRODUCTS_LIST_quantity,
                    productItem.PRODUCTS_List_remark));
            });
        });
        return res;
    }

    /**
     * ajoute un produit à la liste des produits de l'utilisateur
     * @param idProduct identifiant du produit
     * @param idHousehold identifiant du foyer
     * @param quantity quantité choisie par l'utilisateur
     * @param remark le commentaire entré par l'utilisateur
     */
    addProductList(idProduct: number, idHousehold: number, quantity: number, remark: string) {
        return this.httpClient.post<any>('http://localhost:3000/user/' + idHousehold + '/productsList/add', {
            idProd: idProduct,
            quantity: quantity,
            remark: remark
        }).pipe(
            catchError(err => of(false))
        );
    }

    /**
     * met à jour la liste des produits de l'utilisateur
     * @param idProduct identifiant du produit
     * @param idHousehold identifiant du foyer
     * @param quantity quantité du produit
     * @param remark commentaire sur le produit
     */
    updateProductList(idProduct: number, idHousehold: number, quantity: number, remark: string) {
        return this.httpClient.patch<any>('http://localhost:3000/user/' + idHousehold + '/productsList/' + idProduct, {
            quantity: quantity,
            remark: remark
        }).pipe(
            catchError(err => of(false))
        );
    }

    /**
     * permet de supprimer un produit de la liste de l'utilisateur courant
     * @param idProduct identifiant du produit
     * @param idHousehold identifiant du foyer
     */
    deleteProductList(idProduct: number, idHousehold: number) {
        return this.httpClient.delete<any>('http://localhost:3000/user/' + idHousehold + '/productsList/' + idProduct
            , {observe: 'response'}).pipe(
            catchError(err => of(false))
        );
    }

    /**
     *  permet de gérer les erreurs liées au retour des API
     * @param operation méthode appelée
     * @param result résultat renvoyé
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }
}
