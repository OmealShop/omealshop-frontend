import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Apollo } from 'apollo-angular';

import { User } from '../models/user';
import ApolloClient from 'apollo-client';
import gql from 'graphql-tag';

@Injectable()
export class UserService {
    constructor(private apolloInstance: Apollo) { }

    getAll() {
        return this.apolloInstance.watchQuery({
            query: gql`{
                users
            }`
        });
    }

    getById(id: number) {
    }

    register(user: User) {
    }

    update(user: User) {
    }

    delete(id: number) {
    }
}
