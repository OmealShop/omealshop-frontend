import { LayoutModule } from '@angular/cdk/layout';
import { OverlayModule } from '@angular/cdk/overlay';
import { NgModule } from '@angular/core';
import {
    MatButtonModule, MatDialogModule, MatFormFieldModule,
    MatIconModule, MatInputModule,
    MatListModule,
    MatSidenavModule, MatSnackBarModule,
    MatToolbarModule
} from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ApolloModule, APOLLO_OPTIONS } from 'apollo-angular';
import {Form, ReactiveFormsModule} from '@angular/forms';
import { HttpLinkModule, HttpLink } from 'apollo-angular-link-http';

import { FormsModule } from './layout/forms/forms.module';
import { AuthenticationService } from '../app/services/autenthication.service';
import { HttpModule } from '@angular/http';
import { AccueilComponent } from './accueil/accueil.component';
import {SharedModule} from './shared/shared.module';
import {ProductService} from './services/product.service';
import { EditDialogComponent } from './edit-dialog/edit-dialog.component';
// AoT requires an exported function for factories
export const createTranslateLoader = (http: HttpClient) => {
    // for development
    /*return new TranslateHttpLoader(
        http,
        '/start-javascript/sb-admin-material/master/dist/assets/i18n/',
        '.json'
    );*/
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};

@NgModule({
    declarations: [AppComponent, AccueilComponent, EditDialogComponent],
    imports: [
        ApolloModule,
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatFormFieldModule,
        LayoutModule,
        FormsModule,
        ReactiveFormsModule,
        OverlayModule,
        HttpClientModule,
        SharedModule,
        MatInputModule,
        MatButtonModule,
        MatIconModule,
        HttpLinkModule,
        MatSnackBarModule,
        MatDialogModule,
        HttpModule,
        ReactiveFormsModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        })
    ],
    providers: [AuthenticationService, ProductService],
    bootstrap: [AppComponent],
    entryComponents: [EditDialogComponent]
})
export class AppModule {
}
