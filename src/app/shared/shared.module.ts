import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {TopnavComponent} from '../layout/components/topnav/topnav.component';
import {LayoutRoutingModule} from '../layout/layout-routing.module';
import {
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule
} from '@angular/material';
import {TranslateModule} from '@ngx-translate/core';
import {LayoutComponent} from '../layout/layout.component';
import {SidebarComponent} from '../layout/components/sidebar/sidebar.component';

@NgModule({
    imports:      [ CommonModule,
        LayoutRoutingModule,
        MatToolbarModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatListModule,
        TranslateModule ],
    declarations: [ TopnavComponent, LayoutComponent, SidebarComponent],
    exports:      [ TopnavComponent, LayoutComponent, SidebarComponent]
})
export class SharedModule { }
