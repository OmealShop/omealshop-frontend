export class Constants {
    /**
     * récupère l'utilisateur courant
     */
    public static getCurrentUser() {
        return JSON.parse(localStorage.getItem('currentUser'));
    }

    /**
     * récupère l'identifiant du 1er foyer
     */
    public static getFirstUserHouseHold() {
        if (this.getCurrentUser()) {
            return this.getCurrentUser().USER_households[0];
        } else {
            return -1;
        }
    }

    /**
     * récupère le nom de l'utilisateur
     */
    public static getUserName() {
        if (this.getCurrentUser()) {
            return this.getCurrentUser().USER_name;
        } else {
            return '';
        }
    }
}
