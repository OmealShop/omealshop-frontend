import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
    MatButtonModule,
    MatCheckboxModule, MatIconModule,
    MatOptionModule,
    MatSelectModule,
    MatSidenavModule,
    MatSnackBarModule, MatSortModule,
    MatTableModule
} from '@angular/material';
import { MatFormFieldModule, MatPaginatorModule } from '@angular/material';
import { MatInputModule } from '@angular/material';

import { TablesRoutingModule } from './tables-routing.module';
import { TablesComponent } from './tables.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        TablesRoutingModule,
        MatTableModule,
        FormsModule,
        MatSortModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatCheckboxModule,
        MatIconModule,
        MatSidenavModule,
        MatFormFieldModule,
        MatSelectModule,
        MatOptionModule,
        MatPaginatorModule,
        MatInputModule,
        MatSnackBarModule
    ],
    declarations: [TablesComponent]
})
export class TablesModule {}
