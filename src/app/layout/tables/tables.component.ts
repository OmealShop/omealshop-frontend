import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {ProductService} from '../../services/product.service';
import {Product} from '../../models/product';
import {ProductType} from '../../models/producttype';
import {FormBuilder, FormGroup, NgModel, Validators} from '@angular/forms';
import {EditDialogComponent} from '../../edit-dialog/edit-dialog.component';
import {Constants} from '../../shared/constants';

@Component({
    selector: 'app-tables',
    templateUrl: './tables.component.html',
    styleUrls: ['./tables.component.scss']
})
export class TablesComponent implements OnInit {
    readonly userHouseHold = Constants.getFirstUserHouseHold();

    displayedColumns = ['PRODUCT_name', 'PRODUIT_TYPE', 'PRODUCT_QUANTITY', 'PRODUCT_COMMENT', 'actions'];
    dataSource: MatTableDataSource<Product>;
    productListType: ProductType[];
    myProductList: Product[];
    productListItem: Product[] = [];
    filteredArray: Product[] = [];
    selectedProductsId: ProductType[];
    ids: any[] = [];
    form: FormGroup;
    checkedProduct = false;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private productProvider: ProductService, private addProduct: FormBuilder, private snackBar: MatSnackBar,
                private dialog: MatDialog) {
    }

    ngOnInit() {
        this.refreshData();
        this.form = this.addProduct.group({
            quantity: ['', [Validators.required]],
            remark: ['']
        });
    }

    /**
     * filtre le tableau des produits
     * @param filterValue nom du filtre
     */
    applyFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
        this.dataSource.filter = filterValue;
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    /**
     * permet d'afficher les données suivant le filtre
     * @param event évènement généré par Angular
     */
    displayData(event) {
        let array: any;
        array = event;
        if (array.length === 0) {
            this.refreshData();
        } else {
            this.filteredArray = [];
            this.productListType.forEach(product => {
                array.forEach(productId => {
                    if (product.PRODUCTS_id === productId) {
                        product.PRODUCTS_products.forEach(pro => {
                            const userItems = this.productListItem.find(x => x.PRODUCT_id === pro.PRODUCT_id);
                            this.filteredArray.push(new Product(
                                pro.PRODUCT_id,
                                pro.PRODUCT_name,
                                pro.PRODUCT_unit,
                                product.PRODUCTS_title,
                                product.PRODUCTS_id,
                                userItems.PRODUCT_QUANTITY,
                                userItems.PRODUCT_COMMENT));
                        });
                    }
                });
            });
            this.dataSource = new MatTableDataSource<Product>(this.filteredArray);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
        }
    }

    /**
     * supprimer les filtres
     * @param select model contenant les ID des catégories
     */
    emptyFilter(select: NgModel) {
        select.update.emit([]);
        this.displayData([]);
    }

    /**
     * récupère la liste des produits d'un utilisateur
     */
    getMyProductsList() {
        this.myProductList = [];
        this.myProductList = this.productListItem.filter(product => product.PRODUCT_QUANTITY > 0);
        this.dataSource = new MatTableDataSource<Product>(this.myProductList);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
    }

    /**
     * récupère la liste des produits
     */
    getProductsList() {
        this.refreshData();
    }

    /**
     *ouvre une popup permettant de modifier la quantité et le commentaire d'un produit
     * @param PRODUCT_id
     * @param PRODUCT_name
     * @param PRODUCT_QUANTITY
     * @param PRODUCT_COMMENT
     */
    startEdit(PRODUCT_id: number, PRODUCT_name: string, PRODUCT_QUANTITY: any | number, PRODUCT_COMMENT: string | any) {
        const dialogRef = this.dialog.open(EditDialogComponent, {
            data: {productId: PRODUCT_id, productName: PRODUCT_name, quantity: PRODUCT_QUANTITY, comment: PRODUCT_COMMENT}
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result === 1) {
                if (this.checkedProduct) {
                    this.refreshData(true);
                } else {
                    this.refreshData(false);
                }
            }
        });

    }

    /**
     * permet de supprimer un produit
     * @param productId identifiant du produit
     */
    deleteItem(productId: number) {
        this.productProvider.deleteProductList(productId, this.userHouseHold).subscribe(
            result => {
                // @ts-ignore
                if (result.status === 200) {
                    this.snackBar.open('Le produit a bien été supprimé', '', {
                        duration: 2000
                    });
                    if (this.checkedProduct) {
                        this.refreshData(true);
                    } else {
                        this.refreshData(false);
                    }
                } else {
                    this.snackBar.open('Problème lors de la suppression du produit', '', {
                        duration: 2000
                    });
                }
            }
        );
    }

    /**
     * event permet de naviguer entre la liste complète des produits et la liste des produits de l'utilisateur
     * @param evènement envoyé par la détection de l'évènement change
     */
    changeProductList(event) {
        this.checkedProduct = !this.checkedProduct;
        if (event) {
            this.getMyProductsList();
        } else {
            this.getProductsList();
        }
    }

    /**
     * rafraîchit les données du tableau contenant la liste des produits
     * @param filterMyProduct permet de savoir si le filtre sur les produits de l'utilisateur est activé ou non
     */
    refreshData(filterMyProduct = false) {
        this.productListItem = [];
        this.productListType = [];
        this.productProvider.getAllProducts().subscribe(
            r => {
                this.productListType = r['products'] as ProductType[];
                this.productListType.forEach(product => {
                    this.ids.push(product.PRODUCTS_id);
                    product.PRODUCTS_products.forEach(productItem => {
                        this.productListItem.push(new Product(
                            productItem.PRODUCT_id,
                            productItem.PRODUCT_name,
                            productItem.PRODUCT_unit,
                            product.PRODUCTS_title,
                            product.PRODUCTS_id,
                            0,
                            ''));
                    });
                });
                this.productProvider.getProductsHousehold(this.userHouseHold).subscribe(
                    result => {
                        this.productListItem.map(productItem => {
                            result.products.map(productItemR => {
                                if (productItemR.PRODUCTS_LIST_id_prod === productItem.PRODUCT_id) {
                                    productItem.PRODUCT_QUANTITY = productItemR.PRODUCTS_LIST_quantity;
                                    productItem.PRODUCT_COMMENT = productItemR.PRODUCTS_LIST_remark;
                                }
                            });
                        });
                        if (filterMyProduct) {
                            this.getMyProductsList();
                        } else {
                            this.dataSource = new MatTableDataSource<Product>(this.productListItem);
                            this.dataSource.sort = this.sort;
                            this.dataSource.paginator = this.paginator;
                        }
                    });
            });
    }
}
