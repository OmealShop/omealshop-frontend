import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatCheckboxModule, MatInputModule } from '@angular/material';

import { ReactiveFormsModule } from '@angular/forms';
import {SharedModule} from '../shared/shared.module';
import {AccueilComponent} from './accueil.component';

@NgModule({
    imports: [
        CommonModule,
        MatInputModule,
        SharedModule,
        MatCheckboxModule,
        MatButtonModule,
        FlexLayoutModule,
        ReactiveFormsModule
    ],
    declarations: [AccueilComponent]
})
export class AccueilModule {}
