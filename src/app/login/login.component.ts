import {Component, Input, NgZone, OnDestroy, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';


import { AuthenticationService } from '../services/autenthication.service';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
    loginForm: FormGroup;
    submitted = false;
    showError = false;
    response: Subscription;

    constructor(private router: Router, private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService) {}
    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(1)]]
        });
    }

    get f() { return this.loginForm.controls; }

    onLogin() {
        this.submitted = true;
        if (this.loginForm.invalid) {
            return;
        }
        const emailUser = this.loginForm.controls.email.value;
        const passwordUser = this.loginForm.controls.password.value;
        this.response = this.authenticationService.login(emailUser, passwordUser)
        .subscribe(responseFromLogin => {
            if (responseFromLogin) {
                localStorage.setItem('isLoggedin', 'true');
                this.router.navigate(['/dashboard']);
                localStorage.setItem('currentUser', JSON.stringify(responseFromLogin.user));
            } else {
                this.showLoginError();
            }
        });
    }
    showLoginError() {
        this.showError = true;
    }

    ngOnDestroy() {
        if (this.response) {
        this.response.unsubscribe();
    }}
}
