![alt](images/logo-black.png)

# OMealShop

This is the front-end code of OMealShop

## Description

OMealShop is a website that enable to create and manage easily a shopping list for home.

It's possible to  :

- Plan meal for weeks
- Add prodcuts to the home's shopping list
- Add several members of a family to the home
- Create meal or manage those who exists

## Deployment
This is the instructions to deploy the front end code :
- git clone https://gitlab.com/OmealShop/omealshop-frontend
- npm install
- npm start

The website is accessible at localhost:4200

## Environnement
OMealShop-frontend is developped with :
- Angular
- [SB admin material theme](https://startangular.com/product/sb-admin-material/)
- GraphQL API
- Docker

## RoadMap

The roadmap of the project can be viewed [here](https://tree.taiga.io/project/alexispoyen-omealshop/backlog)

List of things to do :

- [ ] Creation of account
- [ ] Creation of home group
- [ ] Manage home group
- [ ] Add product to shopping list
- [ ] Manage meal
- [ ] Plan meal
- [ ] Generate shopping list

## Licence
OMealShop is under GNU GPLv3 licence